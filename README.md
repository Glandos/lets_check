
** DO NOT USE. Prefer https://github.com/Matty9191/ssl-cert-check **

Let's check
===========

Simple and quick script that connect to a TLS service (usually HTTPS web server),
and check if the X.509 certificate is about to expires.

Patch are very welcomed to support other servers, such as STARTTLS services, or
simple file from local filesystem.

Usage
-----
Edit the script to adapt your settings. The following variable can be modified:
 - MAX_REMAINING_DAYS
 - HOSTNAMES
 
Simply launch the script. If everything is OK, nothing is shown, and the return code is 0.
If at least one certificate is about to expire, hostname is written on standard output, and return code is 1.

This design makes it super easy to add as cron task, and get automatic report when needed.