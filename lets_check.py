#!/usr/bin/env python3

import ssl
import socket
import sys

from datetime import datetime, timedelta

MAX_REMAINING_DAYS = 15
HOSTNAMES = ['www.example.com', 'www.example.net']

def get_peer_cert(hostname):
    context = ssl.create_default_context()
    connection = context.wrap_socket(socket.socket(socket.AF_INET), server_hostname=hostname)
    connection.connect((hostname, 443))
    return connection.getpeercert()


def get_remaining(cert):
    not_after = datetime.strptime(cert['notAfter'], '%b %d %H:%M:%S %Y %Z')
    return not_after - datetime.now()


if __name__ == '__main__':
    max_remaining = timedelta(days=MAX_REMAINING_DAYS)
    about_to_expires = []
    for hostname in HOSTNAMES:
        cert = get_peer_cert(hostname)
        if get_remaining(cert) <= max_remaining:
            about_to_expires.append((hostname, cert))

    if about_to_expires:
        for hostname, cert in about_to_expires:
            print(f'{hostname} is about to expire ({cert["notAfter"]})')
        sys.exit(1)

